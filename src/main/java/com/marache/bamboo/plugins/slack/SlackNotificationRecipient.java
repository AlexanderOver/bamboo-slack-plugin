package com.marache.bamboo.plugins.slack;

import com.atlassian.bamboo.deployments.notification.DeploymentResultAwareNotificationRecipient;
import com.atlassian.bamboo.deployments.results.DeploymentResult;
import com.atlassian.bamboo.notification.NotificationRecipient;
import com.atlassian.bamboo.notification.NotificationTransport;
import com.atlassian.bamboo.notification.recipients.AbstractNotificationRecipient;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plugin.descriptor.NotificationRecipientModuleDescriptor;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.template.TemplateRenderer;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;

public class SlackNotificationRecipient extends AbstractNotificationRecipient implements DeploymentResultAwareNotificationRecipient,
                                                                                           NotificationRecipient.RequiresPlan,
        NotificationRecipient.RequiresResultSummary {
    private static final Logger log = Logger.getLogger(SlackNotificationRecipient.class);

    private static final String WEB_HOOK_URL_KEY = "slack_webhookUrl";
    private static final String CHANNEL_KEY = "slack_channel";
    private static final String ICON_URL_KEY = "slack_iconUrl";
    private static final String BOT_NAME_KEY = "slack_botName";

    private String webHookUrl = null;
    private String channel = null;
    private String iconUrl = null;
    private String botName = null;

    private TemplateRenderer templateRenderer;

    private ImmutablePlan plan;
    private ResultsSummary resultsSummary;
    private DeploymentResult deploymentResult;
    private CustomVariableContext customVariableContext;

    @Override
    public void populate(@NotNull Map<String, String[]> params) {
        if (params.containsKey(WEB_HOOK_URL_KEY)) {
            int i = params.get(WEB_HOOK_URL_KEY).length - 1;
            this.webHookUrl = params.get(WEB_HOOK_URL_KEY)[i];
        }
        if (params.containsKey(CHANNEL_KEY)) {
            int i = params.get(CHANNEL_KEY).length - 1;
            this.channel = params.get(CHANNEL_KEY)[i];
        }
        if (params.containsKey(ICON_URL_KEY)) {
            int i = params.get(ICON_URL_KEY).length - 1;
            this.iconUrl = params.get(ICON_URL_KEY)[i];
        }
        if (params.containsKey(BOT_NAME_KEY)) {
            int i = params.get(BOT_NAME_KEY).length - 1;
            this.botName = params.get(BOT_NAME_KEY)[i];
        }
    }

    @Override
    public void init(@Nullable String configurationData) {
        log.info("init " + configurationData);
        if (StringUtils.isNotBlank(configurationData)) {
            String delimiter = "\\|";

            String[] configValues = configurationData.split(delimiter);

            if (configValues.length > 0) {
                webHookUrl = configValues[0];
            }
            if (configValues.length > 1) {
                channel = configValues[1];
            }
            if (configValues.length > 2) {
                iconUrl = configValues[2];
            }
            if (configValues.length > 3) {
                botName = configValues[3];
            }
        }
    }

    @NotNull
    @Override
    public String getRecipientConfig() {
        // We can do this because webhook URLs don't have | in them, but it's pretty dodge. Better to JSONify or something?
        String delimiter = "|";

        StringBuilder recipientConfig = new StringBuilder();
        if (StringUtils.isNotBlank(webHookUrl)) {
            recipientConfig.append(webHookUrl);
        }
        recipientConfig.append(delimiter);
        if (StringUtils.isNotBlank(channel)) {
            recipientConfig.append(channel);
        }
        recipientConfig.append(delimiter);
        if (StringUtils.isNotBlank(iconUrl)) {
            recipientConfig.append(iconUrl);
        }
        recipientConfig.append(delimiter);
        if (StringUtils.isNotBlank(botName)) {
            recipientConfig.append(botName);
        }
        return recipientConfig.toString();
    }

    @NotNull
    @Override
    public String getEditHtml() {
        log.info("descriptor = " + getModuleDescriptor().toString());
        NotificationRecipientModuleDescriptor descriptor = ((NotificationRecipientModuleDescriptor) getModuleDescriptor());
        String editTemplateLocation = descriptor.getEditTemplate();

        log.info("editTemplateLocation = " + editTemplateLocation);

        return templateRenderer.render(editTemplateLocation, populateContext());
    }

    private Map<String, Object> populateContext() {
        Map<String, Object> context = Maps.newHashMap();

        if (webHookUrl != null) {
            context.put(WEB_HOOK_URL_KEY, webHookUrl);
        }
        if (channel != null) {
            context.put(CHANNEL_KEY, channel);
        }

        if (iconUrl != null) {
            context.put(ICON_URL_KEY, iconUrl);
        }

        if (botName != null) {
            context.put(BOT_NAME_KEY, botName);
        }

        log.info("populateContext = " + context.toString());

        return context;
    }

    @NotNull
    @Override
    public String getViewHtml() {
        log.info("descriptor = " + getModuleDescriptor().toString());
        NotificationRecipientModuleDescriptor descriptor = ((NotificationRecipientModuleDescriptor) getModuleDescriptor());
        String viewTemplateLocation = descriptor.getViewTemplate();

        log.info("viewTemplateLocation = " + viewTemplateLocation);

        return templateRenderer.render(viewTemplateLocation, populateContext());
    }


    @NotNull
    public List<NotificationTransport> getTransports() {
        List<NotificationTransport> list = Lists.newArrayList();
        list.add(new SlackNotificationTransport(webHookUrl, channel, iconUrl, botName, plan, resultsSummary, deploymentResult, customVariableContext));
        return list;
    }

    public void setPlan(@Nullable final ImmutablePlan plan) {
        this.plan = plan;
        if (plan != null)
            log.info("set plan to : " + plan.getPlanKey().toString());
    }

    public void setDeploymentResult(@Nullable final DeploymentResult deploymentResult) {
        this.deploymentResult = deploymentResult;
    }

    public void setResultsSummary(@Nullable final ResultsSummary resultsSummary) {
        this.resultsSummary = resultsSummary;
    }

    //-----------------------------------Dependencies
    public void setTemplateRenderer(TemplateRenderer templateRenderer) {
        this.templateRenderer = templateRenderer;
    }

    public void setCustomVariableContext(CustomVariableContext customVariableContext) {
        this.customVariableContext = customVariableContext;
    }
}
