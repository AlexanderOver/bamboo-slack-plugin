# CHANGELOG

## 1.5.10

2020/09/16

- Corrected conflict in ftl template keys thanks to Alexey Chystoprudov.
- Updated bamboo and amps version thanks to Evgeni Gordeev.

## 1.5.9